function Clear-StartMenuLayout()
{
    $layoutPath = "$PSScriptRoot\..\content\empty_start_menu_layout.xml"
    Import-StartLayout -LayoutPath $layoutPath -MountPath "C:\"
    "Cleaned the start menu layout for future users." | Write-Host -ForegroundColor Green
}

Clear-StartMenuLayout
