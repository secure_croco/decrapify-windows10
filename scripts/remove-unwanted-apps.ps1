function Remove-UnwantedApps([string] $SourceFile, [string] $DisplayType)
{
    $names = [IO.File]::ReadAllLines($SourceFile)

    $apps = Get-AppxPackage -AllUsers |
            Where-Object { $_.NonRemovable -eq $false } |
            Where-Object { $_.IsFramework -eq $false }  |
            Where-Object { $names -match $_.Name }

    foreach($app in $apps)
    {
        $name = $app.Name
        Remove-AppxPackage -Package $app -AllUsers

        # The "-Online" switch here means "on the current computer".
        # It doesn't mean "on the internet".
        Get-AppxProvisionedPackage -Online | Where-Object { $_.DisplayName -eq $name } |
        Remove-AppxProvisionedPackage -Online

        "Removed $DisplayType app: $name" |
        Write-Host -ForegroundColor Green
    }
}

$path = "$PSScriptRoot\..\content\unwanted_default_apps.txt"
Remove-UnwantedApps -SourceFile $path -DisplayType "default"

$path = "$PSScriptRoot\..\content\unwanted_extra_apps.txt"
Remove-UnwantedApps -SourceFile $path -DisplayType "extra"
