function Export-RemovableApps([string] $OutPath)
{
    $resultList = New-Object 'System.Collections.Generic.List[string]'

    $apps = Get-AppxPackage -AllUsers |
            Where-Object  { $_.NonRemovable -eq $false } |
            Where-Object  { $_.IsFramework -eq $false }

    foreach($app in $apps)
    {
        $name = $app.Name
        $resultList.Add($name) | Out-Null
    }

    $resultList.Sort()

    [IO.File]::WriteAllLines($OutPath, $resultList)
}

$desktop = [Environment]::GetFolderPath("Desktop")
$path = [IO.Path]::Combine($desktop, "removable_apps.txt")
Export-RemovableApps -OutPath $path
"Exported remaining removable apps to $path" | Write-host -ForegroundColor Green
