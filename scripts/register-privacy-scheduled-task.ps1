function Install-Chocolatey()
{
    $webClient = New-Object System.Net.WebClient
    $scriptUrl = "https://chocolatey.org/install.ps1"
    $scriptContent = $webClient.DownloadString($scriptUrl)
    Invoke-Expression $scriptContent
}

function Install-ShutUp10()
{
    choco install shutup10
}

function Register-PrivacyScheduledTask()
{
    $taskName = "ShutUp10 Apply Privacy Settings"
    $scriptSource = "$PSScriptRoot\..\content\shutup10_template.cfg"
    $scriptDestination = "C:\ProgramData\chocolatey\lib\shutup10\tools\template.cfg"
    $user = "NT AUTHORITY\SYSTEM"
    
    Copy-Item -Path $scriptSource `
              -Destination $scriptDestination `
              -Force
    
    Unregister-ScheduledTask -TaskName $taskName `
                             -Confirm:$false `
                             -ErrorAction SilentlyContinue
    
    $trigger = New-ScheduledTaskTrigger -AtLogOn
    $action = New-ScheduledTaskAction -Execute "oosu10.exe /quiet $scriptDestination"
    
    Register-ScheduledTask -TaskName $taskName `
                           -Trigger $trigger `
                           -Action $action `
                           -User $user `
                           -RunLevel Highest
}

Install-Chocolatey
Install-ShutUp10
Register-PrivacyScheduledTask
